#ifndef GRAFO_H
#define GRAFO_H

#define TRUE 0
#define FALSE 1
#define N 5

class Grafo {
    
    public:
		Grafo();
		void imprimir_grafo(int matriz[N][N], char vector[N]);
};
#endif
