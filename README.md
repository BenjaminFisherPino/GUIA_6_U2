# Guia 6 U2

**Versión 1.0**

Archivo README.md para guía numero 6 de Algoritmos y Estructuras de Datos

Profesor: Alejandro Mauricio Valdes Jimenez

Asignatura: Algoritmos y Estructuras de Datos

Autor: Benjamín Ignacio Fisher Pino

Fecha: 1/11/2021

---
## Resúmen del programa

Este programa fue hecho por Benjamín Fisher para encontrar la ruta mas corta entre N vertices utilizando el algoritmo de Dijkstra.

---
## Requerimientos

Para utilizar este programa se requiere un computador con sistema operativo Linux Ubuntu version 20.04 L, un compilador g++ version 9.3.0, la version 4.2.1 de make y el paquete graphviz. En caso de no tener el compilador de g++, make o graphviz y no saber como descargarlos, abajo se encuentra una pequeña guia de como descargarlos.

### Instalacion de g++

Para instalar g++ debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install g++"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para compilar codigos de c y c++.

### Instalacion de make

Para instalar make debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install make"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar un ejecutable del programa.

### Instalacion de graphviz

Para instalar graphviz debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install graphviz"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar grafos del arbol construido.

---
## Como instalarlo

Para descargar los archivos debe dirigirse al repositorio alocado en la siguiente URL.

Link: https://gitlab.com/BenjaminFisherPino/GUIA_6_U2.git

Tras ingresar al repositorio debe proceder a clonarlo ó a descargar los archivos manualmente, ambas opciones funcionan.Luego debe dirigirse a travez de su terminal a la ruta donde se encuentran los archivos descargados. Una vez ahi, usted debe ingresar a la carpeta  y escribir el siguiente comando en su terminal.

"make" y posteriormente "./proyecto"

En caso de tener problemas asegurece de tener la version 9.3.0 de g++, la version 4.2.1 de make y graphviz instalado.

Puede revisar con "g++ --version" y "make --version" respectivamente.

En el caso de graphviz puede comprobar escribiendo en su terminal "man graphviz", en caso de que no aparezca informacion del paquete debe revisar el tutorial de instalacion denuevo.

Si siguio todos los pasos debiese estar listo para ejecutar el programa!

---
## Funcionamiento del programa

Este programa, desarrollado por Benjamín Fisher, nos permite encontrar la ruta mas corta  entre los N vertices definidos. Se intento añadir la opcion de que el usuario definiese el tamaño de la matriz, pero por una serie de problemas, se prefirio dejar definido como 5. Tras iniciado el programa se nos pedira rellenar la matriz con numeros por teclado, luego de esta accion, se procede a utilizar el algoritmo de Dijkstra para encontrar la ruta mas corta y finalmente se muestra un grafo con la informacion de los vertices. El codigo esta estructurado en base a una funcion Main que nos permite mantener el orden de compilacion y ejecucion mas una clase Grafo, la cual nos permite generar y mostrar el un grafo con informacion respecto a distancia entre los vertices.

---

## License & Copyright

Ⓒ Benjamín Fisher Pino, Universidad de Talca

